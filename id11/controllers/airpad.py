import gevent
import time

from bliss.common.hook import MotionHook
from bliss.common.logtools import log_debug

class WagoInOut(object):
    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.wago = config["wago"]

        detector = config.get("detector", None)
        if detector is not None:
            self._apply_to(detector)

    def _apply_to(self, detector):
        setattr(detector, "insert", self.insert)
        setattr(detector, "remove", self.remove)

    def _set(self, phase):
        channel = self.config[phase]["channel"]
        wait = self.config[phase].get("wait", 0.1)
        log_debug(self, f"start {phase}, pulse on {channel} for {wait} sec.")
        self.wago.set(channel, 1)
        gevent.sleep(wait)
        self.wago.set(channel, 0)
        log_debug(self, f"finished {phase}")

    def insert(self): # pico4 logic...
        self._set("out")

    def remove(self):
        self._set("in")
   
class AirpadWagoPulseHook(MotionHook):
    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.wago = config["wago"]
        super(AirpadWagoPulseHook, self).__init__()
        self.__air_on = False
        self.__manual_set = False
        self.__manual_task = None

    def _add_axis(self, axis):
        if len(self.axes):
            axis_name = list(self.axes.keys())[0]
            raise ValueError(
                "Cannot attach WagoAirpadHook {0!r} to {1!r}. "
                "It is already attached to {2!r}".format(
                    self.name, axis.name, axis_name
                )
            )
        super(AirpadWagoPulseHook, self)._add_axis(axis)

    def set(self, phase):
        self.__air_on = False
        channel = self.config[phase]["channel"]
        wait = self.config[phase].get("wait", 0.1)
        log_debug(self, f"start {phase}, pulse on {channel} for {wait} sec.")
        self.wago.set(channel, 1)
        gevent.sleep(wait)
        self.wago.set(channel, 0)
        log_debug(self, f"finished {phase}")
        if phase == "premove":
            self.__air_on = True

    def pre_move(self, motion_list):
        if not self.__air_on:
            self.set("pre_move")

    def post_move(self, motion_list):
        if not self.__manual_set:
            self.set("post_move")

    def on(self, alert_timeout=10):
        if not self.__air_on:
            self.set("pre_move")
            self.__manual_set = True
        if self.__manual_task is not None:
            self.__manual_task.kill()
        self.__manual_task = gevent.spawn(self.__manual_check, alert_timeout)

    def off(self):
        self.__manual_set = False
        self.set("post_move")
        if self.__manual_task is not None:
            self.__manual_task.kill()

    def __manual_check(self, timeout):
        start_time = time.time()
        gevent.sleep(timeout * 60.0)
        while True:
            elapsed = int((time.time() - start_time) / 60.0)
            print(f"\n!!! WARNING : {self.name} is ON since {elapsed} min. !!!")
            gevent.sleep(60.0)
