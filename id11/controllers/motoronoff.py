import gevent
import time

from bliss.common.hook import MotionHook
from bliss.config.settings import SimpleSetting
from bliss.common.logtools import log_debug


class MotorOnOffHook(MotionHook):
    def __init__(self, name, config):
        super(MotorOnOffHook, self).__init__()
        self.config = config
        self.name = name
        self.__enabled = SimpleSetting(f"{self.name}_enabled", default_value=True)
        self.__scan_on = False

    def __info__(self):
        state = self.__enabled and "ON" or "OFF"
        info = f"{self.name} is {state}"
        if not len(self.axes):
            info += "\nNot defined on any axes yet !!"
        else:
            axlist = ",".join(list(self.axes.keys()))
            info += f"\nApply on axes : {axlist}"
        return info

    def enable(self):
        self.__enabled = True

    def disable(self):
        self.__enabled = False

    def pre_move(self, motion_list):
        if not self.__enabled:
            return
        if not self.__scan_on:
            self.__set_on()

    def post_move(self, motion_list):
        if not self.__enabled:
            return
        if not self.__scan_on:
            self.__set_off()

    def pre_scan(self, axes_list):
        if not self.__enabled:
            return
        self.__scan_on = True
        self.__set_on()

    def post_scan(self, axes_list):
        if not self.__enabled:
            return
        self.__scan_on = False
        self.__set_off()

    def __set_on(self):
        for axis in self.axes.values():
            axis.on()

    def __set_off(self):
        for axis in self.axes.values():
            axis.off()

