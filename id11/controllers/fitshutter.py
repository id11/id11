#!/usr/bin/env python

import sys, numpy as np, pylab as pl



def fitshutter( x, y, exptime, plot_title=None ):
    """
    x = time data
    y = signal data
    exptime = programmed exposure time
    plot_title = None for quiet, a title for your plot    
    """
    yc, xc, s, e = fitshutter_all( x, y )
    opening_time = xc[s>0][0]
    opening_time_err = e[s>0][0]
    closers = xc[s<0]    
    cl_errs = e[s<0]
    m = closers > exptime
    if m.sum() < 2:
        m[-2:]=True
    closing_time = closers[ m ][0] - exptime
    closing_time_err = cl_errs[ m ][0] 
    if plot_title is not None:
        fitshutterplot( x, y, xc, yc, s, opening_time, closing_time,
                        exptime, plot_title)
    return opening_time, closing_time, opening_time_err, closing_time_err



def fitshutter_all( x, y ):
    """
    Finds all the cutting points in a shutter scan
            
    returns: y_cut, xcuts[N], signs[N], xerrs[N]
    """
    first_cut = 0.5*( y.max() + y.min() )     # mid-height
    beam_on  = y[ y > first_cut ]             # y-vals in beam
    beam_off = y[ y < first_cut ]             # y-vals in dark
    avg_on = beam_on.mean()
    avg_off = beam_off.mean()
    y_cut = 0.5*(avg_on + avg_off)            # cut at middle
    beam = 1*(y > first_cut)                  # mask cut on vs off
    crossing = (beam[1:]-beam[:-1])!=0        # spikes where switch on/off
    xlo = x[:-1][crossing]*1.                 # select points at cuts
    xhi = x[1:][crossing]*1.
    ylo = y[:-1][crossing]*1.
    yhi = y[1:][crossing]*1.
    # linear interpolation in the gaps
    grads = (xhi - xlo)/(yhi - ylo)
    x_cut = xlo + (y_cut - ylo)*grads
    signs = np.sign(yhi - ylo)
    cuterr = np.sqrt(beam_on.std()**2 + beam_off.std()**2)/2
    xerrs = cuterr * abs(grads)
    return y_cut, x_cut, signs, xerrs

def fitshutterplot( x, y, xc, yc, s, o, c, expt, fname ):
    """ Makes a nice plot for the shutter fitting """
    import pylab as pl
    pl.figure()
    pl.subplot(211)
    pl.plot( x, y, "+-", label="signal" )
    pl.plot( xc[s>0], [yc,]*len(xc[s>0]), "og", label="Up" )
    pl.plot( xc[s<0], [yc,]*len(xc[s<0]), "or", label="Down" )
    pl.plot( [ o, expt+c] , [yc, yc], "ko", ms=10, label="Chosen" )
    pl.plot(  [0, o] , [yc , yc], "k-")
    pl.plot(  [expt, expt+c] , [yc , yc],"k-")
    pl.text(0,yc," cut=%.1f"%(yc))
    pl.legend(loc=0)
    pl.title( fname )
    ax = pl.subplot(223)
    pl.title("Opening")
    pl.plot( x, y, "+-", label="signal" )
    pl.plot( xc[s>0], [yc,]*len(xc[s>0]), "og", label="Up" )
    pl.plot( xc[s<0], [yc,]*len(xc[s<0]), "or", label="Down" )
    pl.plot(  [0, o] , [yc , yc], "k-")
    pl.plot(  [expt, expt+c] , [yc , yc],"k-")
    pl.text(0,yc*0.4,"%.6f"%(o))
    pl.plot( [0,0], [0,yc*2], "k-" )
    pl.xlim( -o, o*2 )
    ax.ticklabel_format( useOffset = False )
    ax = pl.subplot(224)
    pl.title("Closing")
    pl.plot( x, y, "+-", label="signal" )
    pl.plot( xc[s>0], [yc,]*len(xc[s>0]), "og", label="Up" )
    pl.plot( xc[s<0], [yc,]*len(xc[s<0]), "or", label="Down" )
    pl.plot(  [0, o] , [yc , o], "k-")
    pl.plot(  [expt, expt+c] , [yc , yc],"k-")
    pl.plot( [expt,expt], [0,yc*2], "k-" )
    pl.text(expt,yc*0.4,"%.6f"%(c))
    pl.xlim( expt-c, expt+c*2)
    ax.ticklabel_format( useOffset = False )
    pl.show()
    
def usage():
    print("Usage: %s filename:exptime [filename:exptime ...]"%(sys.argv[0]))
    print("   eg: %s /data/id11/nanoscope/Commissioning/bliss/seb2/pump_on.npy:10"%(
            sys.argv[0]))
    
def main():
    import sys
    if len(sys.argv)==1:
        usage()
    for arg in sys.argv[1:]:
        try:
            fname, expt = arg.split(":")
            expt = float(expt)
            data = np.load( fname )
        except:
            usage()
            raise
        o, c, oe, ce = fitshutter( data['time'], 
                                   data['mon'], 
                                   expt,
                                   plot_title=fname)
        print("%s Open: %f +/- %f , Close %f +/- %f"%( fname, o, oe, c, ce ))
    
if __name__=="__main__":
    main()
    
