# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy as np
from bliss.controllers.motor import CalcController

"""
This is to correct the offset of nfdtx being not parallel to the X-ray beam.

The thy and thx numbers come from align_tdxrd.alignxc macro in bliss tomo.


Example:

controller:
    class: detx
    package: id11.controllers.detx
    x0: 0
    thy: 0.013249064
    thz: -0.02402559
    axes:
        - name: $nfdtx
          tags: real_x
        - name: $d3ty
          tags: real_y
        - name: $d3tz
          tags: real_z
        - name: d3x
          tags: ideal_x
          unit: mm
        - name: d3y
          tags: ideal_y
        - name: d3z
          tags: ideal_z
          
"""


class Detx(CalcController):

    def initialize(self):
        CalcController.initialize(self)
        self._thy = self.config.get("thy", float)
        self._thz = self.config.get("thz", float)
        self._x0  = self.config.get("x0", float)

    def calc_from_real(self, positions_dict):
        tx = positions_dict["real_x"] + self._x0
        ty = positions_dict["real_y"]
        tz = positions_dict["real_z"]
        return        {"ideal_x": tx, 
	                   "ideal_y": ty + tx*np.tan( np.radians( self._thz ) ), # thz is rotation around z
                       "ideal_z": tz - tx*np.tan( np.radians( self._thy ) ), # thz is rotation around z
         }

        
    def calc_to_real(self, positions_dict):
        tx = positions_dict["ideal_x"] - self._x0
        ty = positions_dict["ideal_y"]
        tz = positions_dict["ideal_z"]
        return        {"real_x": tx, 
	                   "real_y": ty - tx*np.tan( np.radians( self._thz ) ), # thz is rotation around z
                       "real_z": tz + tx*np.tan( np.radians( self._thy ) ), # thz is rotation around z
         }

