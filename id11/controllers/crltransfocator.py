import gevent
from bliss.controllers.transfocator import Transfocator
from bliss.common.logtools import log_debug, log_error


class CRLTransfocator(Transfocator):
    def pos_read(self):
        down_stat = self.wago.get("crl_stat_down")
        up_stat = self.wago.get("crl_stat_up")
        log_debug(self, f"pos_read: down={down_stat} up={up_stat}")
        value = 0
        for idx in range(self.nb_lens):
            if down_stat[idx]:
                if up_stat[idx]:
                    # raise RuntimeError(f"CRL [{idx}] both IN and OUT !!")
                    log_error(self, f"CRL [{idx}] both IN and OUT !!")
                value |= 1 << idx
            elif not up_stat[idx]:
                # raise RuntimeError(f"CRL [{idx}] neither IN or OUT !!")
                log_error(self, f"CRL [{idx}] neither IN or OUT !!")
        return value

    def pos_write(self, value):
        log_debug(self, f"pos_write {value}")
        down_stat = self.wago.get("crl_stat_down")
        up_idx = list()
        for idx in range(self.nb_lens):
            if down_stat[idx] and not value & (1 << idx):
                up_idx.append(idx)
        if len(up_idx):
            self.__pulse("crl_cmd_up", up_idx)

        up_stat = self.wago.get("crl_stat_up")
        down_idx = list()
        for idx in range(len(up_stat)):
            if up_stat[idx] and value & (1 << idx):
                down_idx.append(idx)
        if len(down_idx):
            self.__pulse("crl_cmd_down", down_idx)

    def __pulse(self, name, indexes):
        log_debug(self, f"pulse index {indexes} on {name}")
        cmds = [0] * self.nb_lens
        for idx in indexes:
            cmds[idx] = 1
        cmds.reverse()
        self.wago.set(name, *cmds)
        gevent.sleep(0.5)
        cmds = [0] * self.nb_lens
        self.wago.set(name, *cmds)
