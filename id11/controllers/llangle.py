1
import numpy as np
"""
 Compute the true Bragg angles from the various motor positions
 for a single crystal bender and rotation axis.

https://docs.google.com/presentation/d/1lYgPLldDJUbVW9DIwxCdsFKOheoN2SNToVjCANLmb_g/edit?usp=sharing

 Input motor positions are:
  Axis 1:  llrz1 / llbend1 / llty1
  Axis 2:  llrz2 / llbend2 / llty2

 Simple case

  Number being computed:
   - Bragg angle in degrees
"""


from bliss.controllers.motor import CalcController
from bliss.common.logtools import log_info

class LLAngle(CalcController):
    """ mainly for 1st crystal - second crystal will need
    lltx1 ...  use for now anyway
    """
    
    def initialize(self):
        CalcController.initialize(self)
        self.bend_zero  = self.config.get("bend_zero", float)
        self.bend_y  = self.config.get("bend_y", float)
        self.ty_zero = self.config.get("ty_zero", float)

    def check_limits(self, *args, **kwds):
        pass
    
    def calc_from_real( self, positions_dict ):
        log_info(self, "LLAngle calc_from_real()")
        log_info(self, "LLAngle real: %s"%(positions_dict))
        # Angle due to pusher not being a rotation
        # Effect of bending
        # Effect of translation
        bend = positions_dict['bend']
        rz = positions_dict['rz']
        ty = positions_dict['ty']

        truebend = bend - self.bend_zero     # pass through
        absty = ty - self.ty_zero
        bend_offset = np.degrees( truebend * absty / self.bend_y )
        # only for bent crystal and mono in beam
        valid = (truebend > 0) & (absty < 75.)
        angle = np.where( valid, rz + bend_offset, rz )
        calc_dict = { "angle"    : angle , # computed
                      "truebend" : truebend,
                      "absty" : absty,
        }
        log_info(self, "LLangle: %s"%(calc_dict))
        return calc_dict
                      
    def calc_to_real(self, positions_dict ):
        log_info(self, "LLBragg calc_to_real()")
        log_info(self, "LLBragg calc: %s"%(positions_dict))
        #
        angle = positions_dict["angle"]
        # Effect of bending
        truebend = positions_dict[ "truebend" ]
        bend = truebend + self.bend_zero
        # Effect of translation
        absty  = positions_dict["absty"]      # llty1 / llty2
        ty = absty + self.ty_zero
        # Assume we go to the destination ty / bend.
        # Compute the effect for the angle only        
        bend_offset = np.degrees( truebend * absty / self.bend_y )
        # only for bent crystal and mono in beam
        valid = (truebend > 0) & (absty < 75.)
        # - versus + above:
        rz = np.where( valid, angle - bend_offset, angle )
        calc_dict = { "bend" : bend,
                      "ty"   : ty,
                      "rz"   : rz }
        log_info(self,"LLrz: %s"%(str(calc_dict)))
        return calc_dict
        
"""
# ID11 LaueLaue Monochromator

LL_BEND1_FLAT = -0.90
LL_BEND1_BENT = 1.525 #
LL_BEND2_FLAT = 1.000
LL_BEND2_BENT = 3.000 
#                   bent    -   flat
LL_BEND1_ANGDIFF = 3.893346 - 3.8324  # at Sn edge
LL_BEND2_ANGDIFF = 2.58085  - 2.6370 # at Nd edge
"""
        
        
    
