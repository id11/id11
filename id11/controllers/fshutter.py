import gevent

from bliss.config.settings import HashObjSetting
from bliss import current_session

from .fshuttercalib import FastShutterCalibration


class FastShutter:
    def __init__(self, name, config):
        super().__init__()
        self.__name = name
        self.__config = config
        self.__shutter_mux = config["shutter_mux"]
        self.__session_mux = config.get("session_mux", None)
        self.__settings = HashObjSetting(f"fastshutter:{name}")
        self.__time = None
        self.__enabled = self.__settings.get("enabled", True)
        if not self.__enabled:
            self.disable()

        self.__calib = None
        calib_config = config.get("calibration", None)
        if calib_config is not None:
            self.__calib = FastShutterCalibration(self, calib_config)
        
    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    def __info__(self):
        if self.is_enabled():
            scan_state = "YES"
        else:
            scan_state = "NO (shutter DISABLED)"
        info = f"FastShutter [{self.name}]:\n"
        if self.__session_mux is not None:
            info += f"Controlled by session = {self.session}\n"
        info += f"Shutter polarity      = {self.polarity}\n"
        info += f"Shutter mode          = {self.mode}\n"
        info += f"Shutter time          = {self.shutter_time:.3f} sec\n"
        info += f"Shutter used in scans = {scan_state}\n"
        return info

    @property
    def session(self):
        if self.__session_mux is not None:
            return self.__session_mux.getOutputStat("SHUTTER_SESSION")
        else:
            return current_session.name.upper()

    @session.setter
    def session(self, value):
        if self.__session_mux is None:
            raise RuntimeError("No session multiplexer for this shutter")
        self.__session_mux.switch("SHUTTER_SESSION", value.upper())

    def __check_session(self):
        if self.__session_mux is None:
            return
        current = current_session.name.upper()
        if current != self.session:
            print("!!! FastShutter WARNING !!!")
            print(f"Session [{self.session}] is now controlling shutter")
            print("Current session has NO control !!!\n")

    @property
    def polarity(self):
        try:
            return self.__shutter_mux.getOutputStat("SHUTTER_POLARITY")
        except:
            return "NORMAL"

    @polarity.setter
    def polarity(self, value):
        self.__shutter_mux.switch("SHUTTER_POLARITY", value.upper())

    @property
    def shutter_time(self):
        if self.__time is None:
            self.__time = self.__settings.get("time", 0.)
        return self.__time

    @shutter_time.setter
    def shutter_time(self, value):
        self.__settings["time"] = value
        self.__time = None

    @property
    def multiplexer(self):
        return self.__shutter_mux

    def enable(self):
        self.__enabled = True
        self.__settings["enabled"] = True

    def disable(self):
        self.__enabled = False
        self.__settings["enabled"] = False
        self.__shutter_mux.switch("SHUTTER", "CLOSE")

    def is_enabled(self):
        return self.__enabled
    
    def open(self, wait=False):
        self.__check_session()
        #if not self.__enabled:
        #    raise RuntimeError("Shutter is DISABLED. Cannot open it !!")
        self.__shutter_mux.switch("SHUTTER", "OPEN")
        if wait:
            gevent.sleep(self.shutter_time)

    def close(self, wait=False):
        self.__check_session()
        self.__shutter_mux.switch("SHUTTER", "CLOSE")
        if wait:
            gevent.sleep(self.shutter_time)

    @property
    def state(self):
        return self.__shutter_mux.getOutputStat("SHUTTER")

    @property
    def mode(self):
        return self.__shutter_mux.getOutputStat("SHUTTER")

    @mode.setter
    def mode(self, value):
        self.__check_session()
        if not self.__enabled:
            raise RuntimeError("Shutter is DISABLED. Cannot change mode !!")
        self.__shutter_mux.switch("SHUTTER", value.upper())

    @property
    def calibration(self):
        if self.__calib is None:
            raise RuntimeError("No shutter calibration available !!")
        return self.__calib

    def measure(self, open_time=1., acq_time=100e-6, delay=200e-3):
        self.calibration.measure(open_time, acq_time, delay)

    def is_open(self):
        return self.mode == "OPEN"
