import numpy

from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.acquisition.ct2 import CT2AcquisitionMaster,CT2CounterAcquisitionSlave
from bliss.controllers.ct2.device import AcqMode
from bliss.controllers.ct2.client import CT2CounterTimer
from .fitshutter import *

_MUSST_PRG = """
UNSIGNED START_DELAY
UNSIGNED GATEWIDTH
PROG SHUTTER
    CTSTOP TIMER
    CTRESET TIMER
    BTRIG 0       // Close shutter
    STORELIST TIMER
    EMEM 0 AT 0
    @TIMER = START_DELAY
    CTSTART TIMER
    AT TIMER DO STORE ATRIG BTRIG
    @TIMER = $TIMER + GATEWIDTH
    AT TIMER DO BTRIG
ENDPROG
"""

class FastShutterCalibration(object):
    def __init__(self, shutter, config):
        self.__shutter = shutter
        self.__musst = config["musst"]
        self.__p201 = config["p201_card"]
        self.__diode_name = config["p201_diode_name"]

        self.__data = None

    @property
    def data(self):
        return self.__data

    def __check_counters(self):
        self.__diode = None
        self.__timer = None

        for counter in self.__p201.counters:
            if counter.name == self.__diode_name:
                self.__diode = counter
            elif isinstance(counter, CT2CounterTimer):
                self.__timer = counter

        if self.__diode is None:
            raise RuntimeError(f"Could find {self.__diode_name} counter on {self.__p201.name}")

        if self.__timer is None:
            raise RuntimeError(f"Could not find timer on {self.__p201.name}")

    def measure(self, open_time=1., acq_time=100e-6, delay=200e-3):
        self.__check_counters()

        chain = AcquisitionChain()

        musst_time_factor = self.__musst.get_timer_factor()
        musst_master = MusstAcquisitionMaster(self.__musst, program_data=_MUSST_PRG,
                                              program_start_name="SHUTTER",
                                              vars={'START_DELAY':int(delay*musst_time_factor),
                                                    'GATEWIDTH':int(open_time*musst_time_factor)})

        npoints = int((open_time + 2*self.__shutter.shutter_time)/acq_time)
        dead_time = 1e-6
        p201_master = CT2AcquisitionMaster(self.__p201,
                                           npoints=npoints,
                                           acq_mode=AcqMode.ExtTrigSingle,
                                           acq_expo_time=acq_time-dead_time,
                                           acq_point_period=acq_time)
        chain.add(musst_master,p201_master)

        acq_device = CT2CounterAcquisitionSlave(self.__diode, self.__timer,
                                                npoints=npoints, count_time=acq_time)
        chain.add(p201_master, acq_device)

	#calc
        class TimeCumSum(object):
            def __init__(self, cnt_name, dead_time=0.):
                self.previous_time = 0
                self.cnt_name = cnt_name
                self.dead_time = dead_time
            def __call__(self, sender, data_dict):
                data = data_dict.get(self.cnt_name)
                if data is None:
                    return {}
                data = data + self.dead_time
                data = numpy.cumsum(data) + self.previous_time
                self.previous_time = data[-1]
                return {'time': data}

        calc_device = CalcChannelAcquisitionSlave('time_cumsum',(acq_device,),
                                                  TimeCumSum(self.__timer.name, dead_time),
                                                  (AcquisitionChannel('time',numpy.float64,()),))
        chain.add(p201_master, calc_device)

        self.__shutter.mode = "BTRIG"
        mux = self.__shutter.multiplexer
        mux.switch('TRIGGER_MODE','MUSST')
        mux.switch('MUSST','BTRIG',synchronous=True)

        scan_info = {"type": "fshutter_measure"}
    
        scan = Scan(chain, name='fshutter_measure', scan_info=scan_info)
        print("Running fshutter_measure scan ...")
        scan.run()
        print("Scan finished.")
    
        self.__data= scan.get_data()
        time_data = self.__data["time"]
        sig_data = self.__data[self.__diode.fullname]
        print("Running fit procedure ...")
        self.measurement_fit(time_data, sig_data, open_time)
   
    def measurement_fit(self, timedata, sigdata, exp_time): 
        o, c, oe, ce = fitshutter(timedata, sigdata, exp_time, plot_title="Fast Shutter Calibration")
        print("Fit Results:")
        print(f"  Open time  : {o:f} +/- {oe:f} sec")
        print(f"  Close time : {c:f} +/- {ce:f} sec")


