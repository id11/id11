from gevent import sleep
from bliss.controllers.motors.icepap import Icepap
from bliss.common.cleanup import error_cleanup
from bliss.shell.standard import wm

__all__ = [
    "mot_findref",
    "mot_showref",
    "mot_findrange",
    "mot_findhome",
    "mot_findlimit",
]


def mot_findrange(axis, direction=+1):
    if not isinstance(axis.controller, Icepap):
        raise RuntimeError("findmotrange only implemented for icepap motors")
    if direction < 0:
        lim_dir = -1
    else:
        lim_dir = +1

    # --- first limit
    print(
        "Searching {0} hardware limit {1}".format(
            axis.name, lim_dir > 0 and "positive" or "negative"
        )
    )
    mot_findlimit(axis, lim_dir)
    lim_dial1 = axis.limit_found_dial()
    lim_user1 = axis.dial2user(lim_dial1)
    print(f"First limit found at dial={lim_dial1:g} user={lim_user1:g}")
    print()

    # --- second limit
    lim_dir *= -1
    print(
        "Searching {0} hardware limit {1}".format(
            axis.name, lim_dir > 0 and "positive" or "negative"
        )
    )
    mot_findlimit(axis, lim_dir)
    lim_dial2 = axis.limit_found_dial()
    lim_user2 = axis.dial2user(lim_dial2)
    print(f"Second limit found at dial={lim_dial2:g} user={lim_user2:g}")
    print()

    # --- motor travel range
    travel = abs(lim_dial2 - lim_dial1)
    print(f"Motor travel range = {travel:g}")
    print()


def mot_findref(axis):
    if not isinstance(axis.controller, Icepap):
        raise RuntimeError("findmotref only implemented for icepap motors")

    refcfg = _get_motref_config(axis)
    if refcfg["search"] is None or refcfg["dial"] is None:
        raise RuntimeError(
            "Need to configure both reference search method and dial position !!"
        )

    # --- find reference
    print("Start reference search [{0}] ...".format(refcfg["search"]))
    if refcfg["search"] == "HOME+":
        mot_findhome(axis, 1)
        ref_pos = axis.home_found_dial()
    elif refcfg["search"] == "HOME-":
        mot_findhome(axis, -1)
        ref_pos = axis.home_found_dial()
    elif refcfg["search"] == "HOME":
        dial_pos = axis.dial
        # s6d vs s6u in iceid116... still only if above switch??
        if (dial_pos - refcfg["dial"])> 0 and (axis.steps_per_unit > 0):
            home_dir = -1
        else:
            home_dir = 1
        mot_findhome(axis, home_dir)
        ref_pos = axis.home_found_dial()
    elif refcfg["search"] == "LIM+":
        mot_findlimit(axis, 1)
        ref_pos = axis.limit_found_dial()
    elif refcfg["search"] == "LIM-":
        mot_findlimit(axis, -1)
        ref_pos = axis.limit_found_dial()
    print("Reference found at         : {0:g} [dial units]".format(ref_pos))
    print("Reference configured       : {0:g} [dial units]".format(refcfg["dial"]))
    print("Difference with reference  : {0:g}".format(ref_pos - refcfg["dial"]))

    # --- current offset from reference
    offset = axis.dial - ref_pos

    # --- resets dial at reference
    print()
    ans = _yes_no(
        "Do you want to set reference dial position to {0:g}".format(refcfg["dial"]),
        False,
    )
    if ans is True:
        print("==> set dial position to {0:g}".format(refcfg["dial"]))
        axis.dial = refcfg["dial"] + offset
    else:
        print(f"==> dial position unchanged.")

    # --- set limits
    print()
    if refcfg["low_limit"] is None or refcfg["high_limit"] is None:
        print("Reference dial limits not configured. No change.")
    else:
        ans = _yes_no(
            "Do you want to set dial limits to ({0:g}, {1:g})".format(
                refcfg["low_limit"], refcfg["high_limit"]
            ),
            False,
        )
        if ans is True:
            print(
                "==> set limits to ({0:g}, {1:g})".format(
                    refcfg["low_limit"], refcfg["high_limit"]
                )
            )
            low_user = axis.dial2user(refcfg["low_limit"])
            high_user = axis.dial2user(refcfg["high_limit"])
            axis.limits = (low_user, high_user)
        else:
            print("==> motor limits unchanged.")

    # --- user position
    print()
    if refcfg["user"] is None:
        print("Reference user position not configured. No change.")
    else:
        ans = _yes_no(
            "Do you want to set user position to {0:g}".format(refcfg["user"]), False
        )
        if ans is True:
            print("==> user position set to {0:g}".format(refcfg["user"]))
            user_pos = refcfg["user"] + axis.sign * offset
            axis.position = user_pos
        else:
            print("==> user position unchanged.")

    # --- show current config
    wm(axis)


def mot_showref(axis):
    refcfg = _get_motref_config(axis)
    for (key, val) in refcfg.items():
        if val is None:
            refcfg[key] = "NotConfigured"
    print(f"Reference for axis [{axis.name}]")
    print("* reference search method    : {search}".format(**refcfg))
    print("* dial position at reference : {dial}".format(**refcfg))
    print("* dial axis limits           : ({low_limit}, {high_limit})".format(**refcfg))
    print("* user position at reference : {user}".format(**refcfg))
    print()


def mot_findhome(axis, direction=1):
    axis.home(direction, wait=False)
    mot_move_poll(axis)


def mot_move_poll(axis):
    with error_cleanup(axis.stop):
        while axis.is_moving:
            print(
                f"\r{axis.name}: dial={axis.dial:g} user={axis.position:g}",
                end="",
                flush=True,
            )
            sleep(0.1)
    print(
        f"\r{axis.name}: dial={axis.dial:g} user={axis.position:g}", end="", flush=True
    )
    print("")


def mot_findlimit(axis, direction=1):
    axis.hw_limit(direction, wait=False)
    mot_move_poll(axis)


def _yes_no(msg, flag):
    defans = flag is True and "YES" or "NO"
    ask = msg + f" [{defans}] ? "
    ans = input(ask)
    if not len(ans):
        ans = defans
    if ans.upper() in ("YES", "Y"):
        return True
    return False


def _get_motref_config(axis):
    axiscfg = axis.config.config_dict
    refcfg = dict()
    refcfg["search"] = axiscfg.get("reference_search", None)
    refcfg["dial"] = axiscfg.get("reference_dial", None)
    refcfg["user"] = axiscfg.get("reference_user", None)
    refcfg["low_limit"] = axiscfg.get("reference_low_limit", None)
    refcfg["high_limit"] = axiscfg.get("reference_high_limit", None)
    return refcfg
