
from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset

class FScanPreset(ScanPreset):
    def __init__(self, mux, fsh):
        self.mux = mux
        self.fsh = fsh

    def set_fscan_master(self, master):
        self.limadevs = master.lima_used

    def prepare(self, scan):
        if self.fsh.is_enabled():
            self.soft_fsh = True

            for dev in self.limadevs:
                if dev.name.startswith("frelon") and self.soft_fsh is True:
                    if dev.camera.image_mode == "FULL FRAME":
                        dev.shutter.mode = "AUTO_FRAME"
                        dev.shutter.close_time = self.fsh.shutter_time
                        self.soft_fsh = False
                        self.fsh.mode = "FRELON"
                    else:
                        dev.shutter.close_time = 0.0
        else:
            self.soft_fsh = False

        self.mux.switch("TRIGGER", "MUSST")

    def start(self, scan):
        if self.soft_fsh:
            self.fsh.open(wait=True)

    def stop(self, scan):
        if self.soft_fsh:
            self.fsh.close()


class CountPreset(ChainPreset):
    def __init__(self, mux, fsh):
        self.mux = mux
        self.fsh = fsh

    def prepare(self, chain):
        if self.fsh.is_enabled():
            frelonnodes = [
                node for node in chain.nodes_list if node.name.startswith("frelon")
            ]
            if not len(frelonnodes):
                self.fsh.mode = "P201"
                self.soft_fsh = False
            else:
                self.soft_fsh = True
                for node in frelonnodes:
                    dev = node.device
                    if dev.camera.image_mode == "FULL FRAME":
                        dev.shutter.mode = "AUTO_FRAME"
                        dev.shutter.close_time = self.fsh.shutter_time
                        self.soft_fsh = False
                        self.fsh.mode = "FRELON"
                    else:
                        dev.shutter.mode = "MANUAL"
                        dev.shutter.close_time = 0.0
                        self.soft_fsh = True
            self.mux.switch("TRIGGER", "P201")
        else:
            self.soft_fsh = False


    def start(self, chain):
        if self.soft_fsh:
            self.fsh.open(wait=True)

    def stop(self, chain):
        if self.soft_fsh:
            self.fsh.close()
