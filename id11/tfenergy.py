import math
from bliss.setup_globals import *


def tfenergy(nal=None, nbe=None):
    values = tfoh1.getlenses()
    if nal is None:
        nal = values["Al"]
    if nbe is None:
        nbe = values["Be"]

    neff = 1.5682766 * nal + nbe
    print("Energy for EH3 (96 m) : {0:.3f} keV".format(math.sqrt(neff / 0.013981957)))
    print("Energy for EH1 (46 m) : {0:.3f} keV".format(math.sqrt(neff / 0.037651802)))


def tfguess(energy, hutch="EH3"):
    if hutch.upper() not in ["EH1", "EH3"]:
        raise ValueError("hutch parameter should be EH1 or EH3")
    if hutch.upper() == "EH1":
        print("For EH1, try:")
        _albe = 1.5682766
        _const = 0.037651802
        neff = energy * energy * _const
        for nal in range(0, 97, 32):
            if neff > _albe * nal:
                nbe = neff - _albe * nal
                print("{0:d} Al + {1:d} Be".format(nal, int(nbe)))
    else:
        print("For EH3, try:")
        if energy <= 67.:
            nal = 0
            nbe = -1.553 + 0.075578 * energy + 0.013178 * energy * energy
            print("{0:d} Al + {1:d} Be".format(nal, int(nbe)))
        elif energy <= 90:
            nal = 32
            nbe = -73.159 + 0.64454 * energy + 0.009735 * energy * energy
            print("{0:d} Al + {1:d} Be".format(nal, int(nbe)))
        elif energy < 105:
            nal = 64
            nbe = 56.861 - 3.3332 * energy + 0.031577 * energy * energy
            print("{0:d} Al + {1:d} Be".format(nal, int(nbe)))
        elif energy <= 125:
            nal = 96
            nbe = -328.43 + 3.1429 * energy
            print("{0:d} Al + {1:d} Be".format(nal, int(nbe)))
        elif energy > 125:
            print("Sorry, only focuses up to 125 keV !!")
