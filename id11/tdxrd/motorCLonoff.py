import gevent
import time

from bliss.common.hook import MotionHook
from bliss.config.settings import SimpleSetting
from bliss.common.logtools import log_debug


class MotorClosedLoopOnOffHook(MotionHook):
    def __init__(self, name, config):
        super(MotorClosedLoopOnOffHook, self).__init__()
        self.config = config
        self.name = name
        self.__enabled = SimpleSetting(f"{self.name}_enabled", default_value=True)

    def __info__(self):
        state = self.__enabled and "ON" or "OFF"
        info = f"{self.name} is {state}"
        if not len(self.axes):
            info += "\nNot defined on any axes yet !!"
        else:
            axlist = ",".join(list(self.axes.keys()))
            info += f"\nApply on axes : {axlist}"
        return info

    def enable(self):
        self.__enabled = True

    def disable(self):
        self.__enabled = False

    def pre_move(self, motion_list):
        if not self.__enabled:
            return
        for motion in motion_list:
            if motion.axis in self.axes.values():
                self.__set_closed_loop_on(motion.axis)

    def post_move(self, motion_list):
        if not self.__enabled:
            return
        for motion in motion_list:
            if motion.axis in self.axes.values():
                self.__set_closed_loop_off(motion.axis)

    def __set_closed_loop_on(self, axis):
        axis.closed_loop.on()

    def __set_closed_loop_off(self, axis):
        axis.closed_loop.off()

