from bliss.config.static import get_config
from bliss.common.scans import DEFAULT_CHAIN
from bliss.scanning.chain import ChainPreset
from bliss.scanning.scan import ScanPreset


def _get_chain_config(name):
    config = get_config()
    chain_config = config.get(name)
    return chain_config["chain_config"]

def _get_chain_device_config(name, dev):
    config = _get_chain_config(name)
    found_config = None
    for dev_config in config:
        device = dev_config["device"]
        if device == dev:
            found_config = {
                "acquisition_settings": dev_config.get("acquisition_settings", {}),
                "master": dev_config.get("master"),
            }
    return found_config

def set_default_chain():
    DEFAULT_CHAIN.set_settings(_get_chain_config("chain_tdxrd_trig"))
    for dev in DEFAULT_CHAIN._settings:
        try:
             dev.acquisition.mode = "SINGLE"
        except:
             pass

def set_accumulation(limadev, acc_max_expo_time=None):
    dev_config = _get_chain_device_config("chain_tdxrd_acc", limadev)
    if dev_config is not None:
        DEFAULT_CHAIN._settings[limadev] = dev_config
        limadev.acquisition.mode = "ACCUMULATION"
        if acc_max_expo_time is not None:
            limadev.accumulation.max_expo_time = acc_max_expo_time
        else:
            acc_max_expo_time = limadev.accumulation.max_expo_time
        print(f"{limadev.name} set in ACCUMULATION mode with acc_max_expo_time= {acc_max_expo_time} sec")
    else:
        raise RuntimeError(f"Detector {limadev.name} not configured in accumulation chain")
  
def unset_accumulation(limadev):
    dev_config = _get_chain_device_config("chain_tdxrd_gate", limadev)
    if dev_config is not None:
        DEFAULT_CHAIN._settings[limadev] = dev_config
    elif limadev in DEFAULT_CHAIN._settings:
        DEFAULT_CHAIN._settings.pop(limadev)
    limadev.acquisition.mode = "SINGLE"
 
class CountMuxPreset(ChainPreset):
    def __init__(self, opmux, fsh):
        self.opmux = opmux
        self.fsh = fsh

    def prepare(self, chain):
        self.soft_fsh = True
        # --- only one marana
        marananodes = [
            node for node in chain.nodes_list if node.name.startswith("marana")
        ]
        if len(marananodes) > 1:
            raise RuntimeError("Cannot use both marana in the same scan")

        # --- shutter
        frelonnodes = [
            node for node in chain.nodes_list if node.name.startswith("frelon")
        ]
        frelon2shutter = {
            "frelon1": "CAM1", 
            "frelon2": "CAM2", 
            "frelon3": "CAM3",
            "frelon16": "CAM1",
        }
        for node in frelonnodes:
            dev = node.device
            if dev.name in frelon2shutter:
                if dev.camera.image_mode == "FULL FRAME":
                    shutter_mode = frelon2shutter[dev.name]
                    dev.shutter.mode = "AUTO_FRAME"
                    dev.shutter.close_time = self.fsh.shutter_time
                    if self.fsh.is_enabled():
                        self.fsh.mode = shutter_mode
                        self.soft_fsh = False
                    else:
                        print("!!! WARNING !!!")
                        print(f"Your are using frelon {dev.name} in FULL FRAME mode")
                        print("but the fastshutter is DISABLED !!!\n")
                    break
                else:
                    dev.shutter.mode = "MANUAL"
                    dev.shutter.close_time = 0.0

        if not self.fsh.is_enabled():
            self.soft_fsh = False

        # --- triggers
        self.opmux.switch("TRIGGER_MODE", "COUNTER_CARD")
        self.opmux.switch("CAM1", "ON")
        self.opmux.switch("CAM2", "ON")
        self.opmux.switch("CAM3", "ON")
        self.opmux.switch("CAM4", "ON")
        self.opmux.switch("MCA1", "ON")

    def start(self, chain):
        if self.soft_fsh:
            self.fsh.open(wait=True)

    def stop(self, chain):
        if self.soft_fsh:
            self.fsh.close()

CAM2MUX = {
    "frelon1": "CAM1",
    "frelon2": "CAM2",
    "frelon3": "CAM3",
    "marana3": "CAM4",
    "frelon16": "CAM1",
    "marana1": "CAM1",
    "eiger": "CAM3",
}

class FScanMuxPreset(ScanPreset):
    def __init__(self, opmux, fsh):
        self.opmux = opmux
        self.fsh = fsh

    def set_fscan_master(self, master):
        self.limadevs = master.lima_used
        try:
            self.acc_used = master.inpars.lima_acc_used
        except:
            self.acc_used = False

    def prepare(self, scan):
        self.soft_fsh = True
        self.opmux.switch("TRIGGER_MODE", "MUSST")
        if self.acc_used:
            self.opmux.switch("MUSST", "ATRIG")
        else:
            self.opmux.switch("MUSST", "BTRIG")
        self.opmux.switch("MCA1", "ON")

        marana_found = False
        for dev in self.limadevs:
            cam_mux = CAM2MUX[dev.name]
            self.opmux.switch(cam_mux, "ON")
            self.opmux.switch("ITRIG", cam_mux)
            if dev.name.startswith("frelon") and self.soft_fsh is True:
                if dev.camera.image_mode == "FULL FRAME":
                    dev.shutter.mode = "AUTO_FRAME"
                    dev.shutter.close_time = self.fsh.shutter_time
                    if self.fsh.is_enabled():
                        self.fsh.mode = cam_mux
                        self.soft_fsh = False
                    else:
                        print("!!! WARNING !!!")
                        print(f"Your are using frelon {dev.name} in FULL FRAME mode")
                        print("but the fastshutter is DISABLED !!!\n")
                else:
                    dev.shutter.mode = "MANUAL"
                    dev.shutter.close_time = 0.0

            if dev.name[0:6] == "marana":
                if marana_found:
                    raise RuntimeError("Cannot use both marana in the same scan")
                marana_found = True
                dev.camera.output_signal = "FIREROW1"

        if not self.fsh.is_enabled():
            self.soft_fsh = False
                    

    def start(self, scan):
        if self.soft_fsh:
            self.fsh.open(wait=True)

    def stop(self, scan):
        if self.soft_fsh:
            self.fsh.close()

