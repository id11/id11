from bliss.config import static
from bliss.comm import tcp

def stopICEPAPmotors():
    config = static.get_config()
    icepaps = {}
    for name,node in static.ConfigNode.indexed_nodes.items():
        parent = node.parent
        parent_class = parent.get('class')
        if parent_class is None or parent_class.lower() != 'icepap':
            continue

        axis_address = node.get('address')
        if not isinstance(axis_address,int):
            continue

        host = parent.get('host')
        if host is None:
            continue
        
        addresses = icepaps.setdefault(host,set())
        addresses.add(axis_address)

    for ice_host,addresses in icepaps.items():
        cnx = tcp.Socket(ice_host,5000)
        cmd = 'STOP ' + ' '.join(f'{a}' for a in addresses)
        raw_message = cmd.encode()
        raw_message += b'\n'
        cnx.write(raw_message)

def main(args=None):
    stopICEPAPmotors()
        
