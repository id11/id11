from bliss.config.static import get_config
from bliss.common.scans import DEFAULT_CHAIN
from bliss.scanning.chain import ChainPreset
from bliss.scanning.scan import ScanPreset

class CountMuxPreset(ChainPreset):
    def __init__(self, opmux, fsh):
        self.opmux = opmux
        self.fsh = fsh

    def prepare(self, chain):
        self.soft_fsh = True

        if not self.fsh.is_enabled():
            self.soft_fsh = False

        # --- triggers
        self.opmux.switch("TRIGGER_MODE", "COUNTER_CARD")
        self.opmux.switch("CAM1", "ON")
        self.opmux.switch("CAM2", "ON")
        self.opmux.switch("MCA1", "ON")

    def start(self, chain):
        if self.soft_fsh:
            self.fsh.open(wait=True)

    def stop(self, chain):
        if self.soft_fsh:
            self.fsh.close()

CAM2MUX = {
    "basler_eh32": "CAM1",
    "eiger": "CAM2",
}

class FScanMuxPreset(ScanPreset):
    def __init__(self, opmux, fsh):
        self.opmux = opmux
        self.fsh = fsh

    def set_fscan_master(self, master):
        self.limadevs = master.lima_used

    def prepare(self, scan):
        self.soft_fsh = True

        for dev in self.limadevs:
            cam_mux = CAM2MUX.get(dev.name)
            if cam_mux is None:
                raise RuntimeError(f"Camera {dev.name} has no multiplexer. Cannot use it in fscan(s)")
            self.opmux.switch(cam_mux, "ON")
            self.opmux.switch("ITRIG", cam_mux)
            if dev.name == "eiger":
                dev._proxy.saving_statistics_log_enable = True

        self.opmux.switch("TRIGGER_MODE", "MUSST")
        self.opmux.switch("MUSST", "BTRIG")
        self.opmux.switch("MCA1", "ON")

        if not self.fsh.is_enabled():
            self.soft_fsh = False
                    

    def start(self, scan):
        if self.soft_fsh:
            self.fsh.open(wait=True)

    def stop(self, scan):
        if self.soft_fsh:
            self.fsh.close()

