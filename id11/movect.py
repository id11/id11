
from bliss.common.standard import mv, mvr
from bliss.shell.standard import umv, umvr
from bliss.common.scans.ct import ct

__all__ = [
    "mvct",
    "mvrct",
    "umvct",
    "umvrct",
]

global __last_count_time
__last_count_time = 0.1

def mvct(*args, count_time=None):
    mv(*args)
    __move_ct(count_time)

def mvrct(*args, count_time=None):
    mvr(*args)
    __move_ct(count_time)

def umvct(*args, count_time=None):
    umv(*args)
    __move_ct(count_time)

def umvrct(*args, count_time=None):
    umvr(*args)
    __move_ct(count_time)

def __move_ct(count_time):
    global __last_count_time
    if count_time is None:
        try:
            from bliss import current_session
            last_scan = current_session.scans[-1]
            count_time = last_scan.scan_info["count_time"]
            __last_count_time = count_time
        except:
            count_time = __last_count_time
    ct(count_time)

