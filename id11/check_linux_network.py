

import subprocess, os

HOSTS = [
    'lid11frelon1',
    'lid11deb1',
    'lid11deb2',
    'lid11eh3',
    'lid11bas1',
    'lid11bas2',
    'lid111',
    'lid112',
    ]

PASSWORD = None

def check_host(hostname):
    CMD = "/sbin/ifconfig"
    ssh = subprocess.Popen(["sshpass", "-p",PASSWORD,"ssh",
                            "opid11@%s"%(hostname), CMD],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE, )
    result = [l.decode('utf-8') for l in ssh.stdout.readlines()]
    error = ssh.stderr.readlines()
    return result, error


def interpret(r, e):
    bad = {}
    for line in r:
        if len(line) and line[0]!=" ":
            iface = line.strip()
        if line.find("rrors")>0:
            for token in line.split()[2:]:
                name, val = token.split(":")
                if int(val.strip())>0:
                    bad[iface] = line.rstrip()
    return bad



def check_linux_network():
    global PASSWORD
    if PASSWORD is None:
        print("Set check_linux_network.PASSWORD='secret' and try again")
        return
    nbad = 0
    for hostname in HOSTS:
        print("Checking",hostname)
        bad = interpret( *check_host( hostname ) )
        if len(bad):
            nbad += 1
            print(hostname)
            for k in bad:
                print(k)
                print(bad[k])
    print("All done, back to you",nbad,"machines are sick")
    return nbad

__all__ = [check_linux_network, ]
