import gevent
import numpy
from bliss.shell.standard import flint
from bliss.common.counter import SoftCounter
from bliss.common.regulation import ExternalInput


class StressInput(ExternalInput):
    def __init__(self, config):
        super().__init__(config)
        
        self.device = config['device']
        self.adc_channel = config['channel']

    def read(self):
        """ returns the input device value (in input unit) """

        return float(self.device.controller.command(f"TSP? {self.adc_channel}"))

    def state(self):
        """ returns the input device state """

        log_debug(self, "StressInput:state")
        return "READY"


STRESS_RIG_MOTOR=None

def create_counter(motor,channel):
    def read_ADC():
        return float(motor.controller.command(f"TSP? {channel}"))
    return SoftCounter(name="stress_adc",value=read_ADC)
        

def init(motor,offset,gain):
    """
    This function should be call in setup.
    """
    global STRESS_RIG_MOTOR
    STRESS_RIG_MOTOR=motor
    #init motor
    motor.position
    #init the channel
    ctrl = motor.controller
    ctrl.command("CCL 1 advanced")
    #ctrl.command("SPA 2 0x02000300 25.7")   # 25.7
    #trl.command("SPA 2 0x02000200 -184.9")
    ctrl.command(f"SPA 2 0x02000300 {gain}")
    ctrl.command(f"SPA 2 0x02000200 {offset}")

    class Fake:
        name='Analog_input'
        channel = 2
    fake = Fake()
    motor.controller.axes[fake.name] = fake
    #Record the axis position
    motor.controller.set_recorder_data_type(motor,motor.VOLTAGE_OF_OUTPUT_CHAN,fake,
                                            motor.SENSOR_MECHLINEAR_OF_INPUT_CHAN)
                                            #motor.CONTROL_VOLTAGE_OF_OUTPUT_CHAN)#CURRENT_POSITION_OF_AXIS)
    
def stress_run_display(wavetype,offset,amplitude,nb_cycles,wavelen,recorder_rate=None):
    if STRESS_RIG_MOTOR is None:
        raise RuntimeError("First call init")

    mot = STRESS_RIG_MOTOR
    # Starts the recorder when the trajectory starts
    mot.controller.start_recording(mot.controller.WAVEFORM,recorder_rate=recorder_rate)

    def refresh_display(data):
        current_index = len(data) if data is not None else 0
        ldata = mot.controller.get_data(from_event_id=current_index)
        if ldata is None or len(ldata) == 0:
            return data
        if data is not None:
            data = numpy.append(data,ldata)
        else:
            data = ldata

        x,y,y2 = (data[name] for name in data.dtype.names)
        p.clear_data()
        #p.add_curve(x, y, legend='target')
        p.add_curve(x, y2, legend='measured')
        #p.plot(data=y,x=x)
        #p.set_data(data={'target':y,'current':y2},x=x)
        return data
    
    # Get flint
    f = flint()
    #Create the plot
    p = f.get_plot("curve",name="stress",unique_name="pi_stress")
    data=None
    with mot.run_wave(wavetype,offset,amplitude,nb_cycles,wavelen):
        while not mot.state.READY:
            current_index = len(data) if data is not None else 0
            data = refresh_display(data)
            if current_index == len(data):
                gevent.sleep(1)

    #last display
    refresh_display(data)
    


