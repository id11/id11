import gevent
import time

from fscan.ftimescan import FTimeScanCustomRunner
from fscan.fscan import FScanCustomRunner
from fscan.fscan2d import FScan2DCustomRunner
from fscan.finterlaced import FInterlacedMode, FInterlacedCustomRunner
from fscan.fsweep import FSweepCustomRunner
from fscan.f2scan import F2ScanCustomRunner


def set_frelon_shutter_time(devs, shuttime):
    for dev in devs:
        camtype = dev._proxy.lima_type
        if camtype == "Frelon":
            if dev.camera.image_mode == "FULL FRAME":
                dev.shutter.close_time = shuttime
            else:
                dev.shutter.close_time = 0.0

def get_default_scan_mode(devs):
    pars = {"scan_mode": "TIME"}
    if len(devs) == 1:
        dev = devs[0]
        if dev.name == "eiger":
            pars["scan_mode"] = "CAMERA"
            pars["camera_signal"] = "LEVEL"
        elif dev.name[0:6] == "marana":
            pars["scan_mode"] = "CAMERA"
            pars["camera_signal"] = "EDGE"
    return pars

def check_camera_scan_mode(devs):
    for dev in devs:
        if dev.name.startswith("basler"):
            return False
        if dev.name[0:6] == "marana":
            dev.camera.overlap = "ON"
    return True


class ID11FTimeScan(FTimeScanCustomRunner):
    def __init__(self, scanname, scanmaster, fshutter):
        self._fsh = fshutter
        super().__init__(scanname, scanmaster)

    def local_validate(self):
        limadevs = self._master.get_controllers_found("lima")
        set_frelon_shutter_time(limadevs, self._fsh.shutter_time)

        if self.pars.scan_mode == "CAMERA":
            if check_camera_scan_mode(limadevs) is False:
                self.pars.scan_mode = "TIME"

    def get_local_pars(self):
        limadevs = self._master.get_controllers_found("lima")
        return get_default_scan_mode(limadevs)


class ID11FScan(FScanCustomRunner):
    def __init__(self, scanname, scanmaster, fshutter):
        self._fsh = fshutter
        super().__init__(scanname, scanmaster)

    def local_validate(self):
        limadevs = self._master.get_controllers_found("lima")
        set_frelon_shutter_time(limadevs, self._fsh.shutter_time)

        if self.pars.scan_mode == "CAMERA":
            if check_camera_scan_mode(limadevs) is False:
                self.pars.scan_mode = "TIME"

    def get_local_pars(self):
        limadevs = self._master.get_controllers_found("lima")
        return get_default_scan_mode(limadevs)

class ID11F2Scan(F2ScanCustomRunner):
    def get_local_pars(self):
        limadevs = self._master.get_controllers_found("lima")
        return get_default_scan_mode(limadevs)


class ID11FScan2D(FScan2DCustomRunner):
    def __init__(self, scanname, scanmaster, fshutter):
        self._fsh = fshutter
        super().__init__(scanname, scanmaster)

    def local_validate(self):
        limadevs = self._master.get_controllers_found("lima")
        set_frelon_shutter_time(limadevs, self._fsh.shutter_time)

        if self.pars.scan_mode == "CAMERA":
            if check_camera_scan_mode(limadevs) is False:
                self.pars.scan_mode = "TIME"

    def get_local_pars(self):
        limadevs = self._master.get_controllers_found("lima")
        return get_default_scan_mode(limadevs)


class ID11FSweep(FSweepCustomRunner):
    def __init__(self, scanname, scanmaster, fshutter):
        self._fsh = fshutter
        super().__init__(scanname, scanmaster)

    def local_validate(self):
        pars = self.pars

        limadevs = self._master.get_controllers_found("lima")
        set_frelon_shutter_time(limadevs, self._fsh.shutter_time)


class ID11FInterlaced(FInterlacedCustomRunner):
    def __init__(self, scanname, scanmaster, fshutter):
        self._fsh = fshutter
        super().__init__(scanname, scanmaster)

    def local_validate(self):
        pars = self.pars

        # --- frelon shutter time checks
        limadevs = self._master.get_controllers_found("lima")
        set_frelon_shutter_time(limadevs, self._fsh.shutter_time)
        frelons = [ dev for dev in limadevs if dev._proxy.lima_type == "Frelon" ]

        # --- shift_pos adjusted in zigzag mode with frelon
        pars.shift_pos = 0.
        if pars.mode == FInterlacedMode.ZIGZAG and len(frelons):
            dev = frelons[0]
            if dev.camera.image_mode != "FRAME TRANSFER":
                shut_time = self._fsh.shutter_time
                mot_speed = abs(pars.acq_size) / pars.acq_time
                shut_pos = shut_time * mot_speed
                if pars.acq_size > 0:
                    direction = 1.
                else:
                    direction = -1.
                pars.shift_pos = direction * shut_pos / 2.
                 

