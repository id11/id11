



def icepos_log(quiet=True):
    import time
    from bliss import current_session, global_map
  
    logpath = "/users/opid11/logfiles/"
    logname = current_session.name + time.strftime("%Y%m%dT%Hh%M")
    print("Logging motor positions to:",logpath+logname)
    with open(logpath+logname,"a") as f:
        for ax in global_map.get_axes_iter():
            if hasattr(ax.controller, "read_encoder_all_types" ):
                msg = "Name %s user %f dial %f limits %f %f %s\n"%(
                    ax.name,
                    ax.position,
                    ax.dial,
                    ax.limits[0],
                    ax.limits[1],
                    ax.controller.read_encoder_all_types(ax),
                )
                f.write( msg )
                if not quiet:
                    print(msg,end=" ")
            
