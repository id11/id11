
from bliss.config.static import get_config


def desc(*objs):
    for obj in objs:
        try:
            config = get_config()
            objcfg = config.get_config(obj.name)
            desc = objcfg.get("description", "no description.")
            print(f"{obj.name} : {desc}")
        except:
            print(f"Failed to get config for {obj}")
