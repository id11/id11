# -*- coding: utf-8 -*-

"""The setup script."""

import sys
from setuptools import setup, find_packages

TESTING = any(x in sys.argv for x in ['test', 'pytest'])

with open('README.rst') as readme_file:
    readme = readme_file.read()

requirements = ['bliss']

setup_requirements = ['pytest-runner', 'pytest'] if TESTING else []

test_requirements = ['pytest-cov', 'mock']

console_script_entry_points = ["stopICEPAPmotors = id11.scripts.stop_icepap:main"]

setup(
    author="BCU Team",
    author_email='papillon@esrf.fr',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    description="ID11 bliss software",
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme,
    include_package_data=True,
    keywords='id11',
    name='id11',
    entry_points={"console_scripts": console_script_entry_points},
    packages=find_packages(include=['id11']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.esrf.fr/ID11/id11',
    version='0.1.0',
    zip_safe=False,
)
