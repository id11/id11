=============
ID11 project
=============

[![build status](https://gitlab.esrf.fr/ID11/id11/badges/master/build.svg)](http://ID11.gitlab-pages.esrf.fr/ID11)
[![coverage report](https://gitlab.esrf.fr/ID11/id11/badges/master/coverage.svg)](http://ID11.gitlab-pages.esrf.fr/id11/htmlcov)

ID11 software & configuration

Latest documentation from master can be found [here](http://ID11.gitlab-pages.esrf.fr/id11)
